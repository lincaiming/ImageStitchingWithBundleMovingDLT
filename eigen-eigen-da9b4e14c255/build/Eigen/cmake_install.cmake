# Install script for directory: /home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE FILE FILES
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/QR"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/Householder"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/LU"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/SuperLUSupport"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/CholmodSupport"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/MetisSupport"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/SparseLU"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/Core"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/Cholesky"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/Eigenvalues"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/PardisoSupport"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/Dense"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/SparseCholesky"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/OrderingMethods"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/Jacobi"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/SVD"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/SparseCore"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/StdList"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/Sparse"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/SparseQR"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/Eigen"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/Geometry"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/StdVector"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/UmfPackSupport"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/PaStiXSupport"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/StdDeque"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/SPQRSupport"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/IterativeLinearSolvers"
    "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/QtAlignedMalloc"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE DIRECTORY FILES "/home/ming/SoftWare/eigen-eigen-da9b4e14c255/Eigen/src" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

