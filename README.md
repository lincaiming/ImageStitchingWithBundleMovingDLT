### As-Projective-As-Possible Image Stitching with Moving DLT
Ubuntu16.04 gcc-5.4  

Compatibility: The code was tested with MATLAB 2013 and 2014. MATLAB 2015 changed
its pooling functions, so you may have to change the code if you receive an error message when running in MATLAB 2015.
Dependencies: You will need to install EIGEN and Google's Ceres solver and link them to your
MATLAB command line when you build the corresponding MEX files.


if you meet some error about libraries error,don't worry ,please Google to solve it.

modified author: lincaiming
email: lincaiming5837@gmail.com
date: 2017-02-14